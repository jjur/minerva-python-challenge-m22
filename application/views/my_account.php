<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>

<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-6 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Minerva Hall of Code</span></h1>


            <div class="flex mb3">
                <div class="">
                    <p class="h5">Please use your name to post message to Hall of Code(Fame). Please wait for loading, it may take 30 seconds. Use "Pridať odkaz" to write new message.</p>
                    <form method="post" action="<?php echo base_url(); ?>account/login">
                        <iframe frameborder="0" scrolling="no" width="500" height="500" src="http://miniaplikace.blueboard.cz/shoutboard.php?hid=v4ss6fri9orp4du9ck5mrf0m3mod36">Please wait for loading
                            <a href="http://miniaplikace.blueboard.cz/shoutboard.php?hid=v4ss6fri9orp4du9ck5mrf0m3mod36">ShoutBoard od BlueBoard.cz</a>
                        </iframe>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Main -->

<!-- FOOTER -->
<footer>
    <section class="py1 white bg-whiteish">
        <div class="container">
            <div class="clearfix mt2 mb4">
                <div class="md-col md-col-8 border-top border-lightest">
                    <div class="mt1">
                        <div class="h3 mid serif black inline-block mr1">Stay in touch</div>
                        <div class="mt2 mb2 inline-block ml1">
                            <a href="https://www.minerva.kgi.edu/connect" class="h5 btn btn-primary bg-lightest hover-bg-accent-light whiteish rounded px3 regular"><span class="text-smaller px1">Sign up</span></a>
                        </div>
                    </div>
                </div>
                <div class="md-col md-col-1 undo-collapse"></div>
                <div class="md-col md-col-3">
                    <div style="border-top-width:12px;" class="border-top border-lightest">
                        <div class="uppercase h5 serif mb1 mid black"><span class="text-smaller">Follow Us/</span></div>
                        <div class="clearfix flex">
                            <a class="light cursor-pointer hover-mid" href="https://www.facebook.com/pages/Minerva-Schools-at-KGI/1417545631800995" target="_blank" title="Minerva on Facebook">
                                <h3 class="mt0 no-leading-regular"><i class="ui ui-social-facebook" aria-hidden="true"></i></h3>
                            </a>
                            <div class="flex undo-collapse flex-auto"></div>
                            <a class="light cursor-pointer hover-mid" href="https://instagram.com/minervaschools/" target="_blank" title="Minerva on Instagram">
                                <h3 class="mt0 no-leading-regular"><i class="ui ui-social-instagram" aria-hidden="true"></i></h3>
                            </a>
                            <div class="flex undo-collapse flex-auto"></div>
                            <a class="light cursor-pointer hover-mid" href="https://twitter.com/MinervaSchools" target="_blank" title="Minerva on Twitter">
                                <h3 class="mt0 no-leading-regular"><i class="ui ui-social-twitter" aria-hidden="true"></i></h3>
                            </a>
                            <div class="flex undo-collapse flex-auto"></div>
                            <a class="light cursor-pointer hover-mid" href="https://www.youtube.com/user/minerva" target="_blank" title="Minerva on YouTube">
                                <h3 class="mt0 no-leading-regular"><i class="ui ui-social-youtube" aria-hidden="true"></i></h3>
                            </a>
                            <div class="flex undo-collapse flex-auto"></div>
                            <a class="light cursor-pointer hover-mid" href="https://www.linkedin.com/school/15094841/" target="_blank" title="Minerva on Linkedin">
                                <h3 class="mt0 no-leading-regular"><i class="ui ui-social-linkedin" aria-hidden="true"></i></h3>
                            </a>
                            <div class="flex undo-collapse flex-auto"></div>
                            <a class="light cursor-pointer hover-mid" href="https://medium.com/minerva-schools" target="_blank" title="Minerva on Medium">
                                <h3 class="mt0 no-leading-regular"><i class="ui ui-social-medium" aria-hidden="true"></i></h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section style="background:#1e1914;" class="py1 white">
        <div class="container">
            <div class="clearfix mt2 mb2">
                <div class="md-col md-col-4">
                    <div class="mr1 border-top border-lightest">
                        <ul class="list-reset">
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="https://www.minerva.kgi.edu/academics/" title="Academic Programs"><h6 class="thin">Academic Programs</h6></a></li>
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="https://www.minerva.kgi.edu/global-experience/" title="Global Experience"><h6 class="thin">Global Experience</h6></a></li>
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="https://www.minerva.kgi.edu/career-development/" title="Career Development"><h6 class="thin">Career Development</h6></a></li>
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="https://www.minerva.kgi.edu/admissions/" title="Admissions"><h6 class="thin">Admissions</h6></a></li>
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="https://www.minerva.kgi.edu/tuition-aid/" title="Tuition &amp; Aid"><h6 class="thin">Tuition &amp; Aid</h6></a></li>
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="https://www.minerva.kgi.edu/about/" title="About Minerva"><h6 class="thin">About Minerva</h6></a></li>
                        </ul>
                    </div>
                </div>
                <div class="md-col md-col-4">
                    <div class="mr1 border-top border-lightest">
                        <ul class="list-reset">
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="https://www.minerva.kgi.edu/frequently-asked-questions" title="Frequently Asked Questions"><h6 class="thin">Frequently Asked Questions</h6></a></li>
                            <!-- TODO: Add Calendar
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="#" title="Calendar"><h6 class="thin">Calendar</h6></a></li> -->
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="https://www.minerva.kgi.edu/people" title="People"><h6 class="thin">People</h6></a></li>
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="https://www.minerva.kgi.edu/careers" title="Careers"><h6 class="thin">Careers</h6></a></li>
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="https://www.minerva.kgi.edu/resources" title="Resources"><h6 class="thin">Resources</h6></a></li>
                            <li class="mt2"><a class="cursor-pointer whiteish hover-accent-light text-decoration-none" href="https://www.minerva.kgi.edu/contact" title="Contacts"><h6 class="thin">Contacts</h6></a></li>
                        </ul>
                    </div>
                </div>
                <div class="md-col md-col-1 undo-collapse"></div>
                <div class="md-col md-col-3">
                    <div style="border-top-width:12px;" class="border-top border-light">
                        <div class="uppercase h6 serif py1">Admissions Office /</div>
                        <div class="mt2 h5 serif">1145 Market St,&nbsp;&nbsp;9th Floor<br>San Francisco, CA<br>94103 USA<br><a href="mailto:admissions@minerva.kgi.edu" class="orange">admissions@minerva.kgi.edu</a></div>
                    </div>
                </div>
            </div>
            <div class="clearfix mt1">
                <div class="md-col md-col-4">
                    <div class="mr1 border-top border-light">
                        <div class="py1 mb2 h6 thin light no-leading">©2017 Minerva Project, Inc. All Rights Reserved.</div>
                    </div>
                </div>
                <div class="md-col md-col-4">
                    <div class="mr1 border-top border-light">
                        <div class="py1 mb2 col col-4 h6 thin no-leading"><a href="https://www.minerva.kgi.edu/terms" class="light">Terms &amp; Conditions</a></div>
                        <div class="py1 mb2 col col-4 h6 thin no-leading"><a href="https://www.minerva.kgi.edu/privacy" class="light">Privacy Policy</a></div>
                        <div class="py1 mb2 col col-4 h6 thin no-leading"><a href="https://www.minerva.kgi.edu/cookies" class="light">Cookie Declaration</a></div>
                    </div>
                </div>
                <div class="md-col md-col-1 undo-collapse"></div>
                <div class="md-col md-col-3">
                    <div class="border-top border-light">
                        <div class="md-col md-col-5 undo-collapse"></div>
                        <div class="col col-5 md-col-7 mb4">
                            <a href="http://www.kgi.edu/"><img class="full-width" src="<?php echo base_url(); ?>/assets2/logo-KGI.svg" alt="KGI logo"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>

<script src="<?php echo base_url(); ?>/assets2/cms_gallery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets2/bba96727.js"></script>      <!-- END FOOTER -->


<!--[if lt IE9]>
<script src="http://minerva.kgi.edu/public/js/html5.js"></script>
<script src="http://minerva.kgi.edu/public/js/respond.js"></script>
<![endif]-->


<!-- swiftype search box -->
<script type="text/javascript">
    (function(w,d,t,u,n,s,e){w['SwiftypeObject']=n;w[n]=w[n]||function(){
        (w[n].q=w[n].q||[]).push(arguments);};s=d.createElement(t);
        e=d.getElementsByTagName(t)[0];s.async=1;s.src=u;e.parentNode.insertBefore(s,e);
    })(window,document,'script','//s.swiftypecdn.com/install/v2/st.js','_st');

    _st('install','TMVzky1S_9_z7jFBoarD','2.0.0');
</script>
<script src="<?php echo base_url(); ?>/assets2/16d8cc00.js">
</script>

<script src="<?php echo base_url(); ?>/assets2/commons_navigation.js" type="text/javascript"></script>


</div></div><link rel="stylesheet" href="<?php echo base_url(); ?>/assets2/woff2.css" media="all">


<div class="st-injected-content-generated st-install-TMVzky1S_9_z7jFBoarD" data-st-install-key="TMVzky1S_9_z7jFBoarD" id="st-injected-content">
    <div class="st-ui-embed st-search-chrome-overlay-output st-search-escape-hide-outputs st-search-zoom-on-mobile" style="display: none;">
        <div class="st-ui-overlay st-search-hide-outputs"></div>
        <!-- Swiftype input and results overlay -->
        <div class="st-ui-container st-ui-container-overlay  st-ui-injected-overlay-container ">
            <!-- HEADER -->
            <div class="st-ui-container-header_bar st-position-container">
                <section class="st-ui-header">
                    <form>
                        <input class="st-default-search-input st-search-set-focus" type="text" placeholder="Search this site" id="st-overlay-search-input" autocomplete="off" autocorrect="off" autocapitalize="off">
                    </form>
                    <span class="st-ui-search-icon"></span>

                    <a class="st-ui-close-button st-search-hide-outputs">Close</a>

                </section>
            </div>

            <!-- LEFT SIDEBAR -->
            <div class="st-ui-container-left_sidebar st-position-container">
                <div class="st-ui-injected-search-controls">
                </div>
            </div>

            <!-- RIGHT SIDEBAR -->
            <div class="st-ui-container-right_sidebar st-position-container">
                <div class="st-ui-injected-container-right_sidebar">
                    <div class="st-ui-injected-search-controls">
                    </div>
                </div>
            </div>

            <!-- PRIMARY CONTENT -->
            <div class="st-ui-container-primary_content st-position-container">
                <section class="st-ui-slide-autocomplete st-autocomplete-keyboard-navigable st-autocomplete-transient-on-select-and-results" data-st-target-element="#st-overlay-search-input" data-st-active-query-class="active">
                    <span class="st-ui-type-heading-small">suggested results</span>
                    <div class="st-autocomplete-results"><div class="st-query-present" style="display: none;"></div></div>
                </section>

                <section class="st-ui-content st-search-results"><div class="st-query-not-present">
                        <span class="st-ui-fallback"></span>
                    </div><div class="st-query-present" style="display: none;"></div></section>

                <section class="st-ui-no-results st-search-suggestions"><div class="st-query-not-present">
                    </div></section>
            </div>



            <!-- FOOTER -->
            <div class="st-ui-container-footer_bar st-position-container">
                <section class="st-ui-footer">
                    <span class="st-ui-search-summary st-search-summary"></span>
                    <span class="st-ui-pagination st-search-pagination"></span>
                </section>
            </div>
        </div>
    </div>
    <div class="st-default-autocomplete" data-st-target-element=".st-default-search-input" style="display: none;">
        <div class="st-autocomplete-results st-ui-autocomplete"><div class="st-query-present" style="display: none;"></div></div>
    </div>
</div></body></html>