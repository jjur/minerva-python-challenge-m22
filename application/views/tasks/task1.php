<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 1</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Welcome! The Product Development Team is working on migrating ALF to new core code in
                        Python. You have been invited to help ALF dev team with migration of old code.</p>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                    <p class="h5">Your first task will be to calculate in Python3 the number DATA in bits (b) that needs the ALF
                        server to
                        handle per 24 hours. Each open ALF during class transfers 1.5 GB data per class. There are 2 classes
                        in average per 24 hours. And 400 students every day. There 2 available servers. The requests are
                        equally split between servers. Submit result as integer (0 decimal places, no rounding)</p>
                    <p><b>Use conversion:</b></p>
                    <p>1 GB = 1024 MB</p>
                    <p>1 MB = 1024 kB</p>
                    <p>1 kB = 1024 B</p>
                    <p>1 B = 8 b</p>
                    <p>
                    </p>


                    <p></p>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>


                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
