<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 2</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Good Job! Your computation is right!</p>
                    <br>



                    <p>The core feature of ALF requires to do big computation. Compute in Python 400^(2018+2019).</p>
                    <p>HINT: It is a big number :). Python is really great in computing a great numbers</p>
                    <p>HINT2: This server is not using Python and do not like big numbers. :) If you see error URL is too large, delete all 0 from end of your number. Example: 102340056000000 to 102340056.</p>



                    <p></p>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>


                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
