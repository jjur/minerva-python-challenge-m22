<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 12</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Good Job! You correctly identified correct slicing of given list of students.</p>
                    <pre><code class="python">
#index of element |   0        1        2        3          4            5       6        7
#index of element |  -8       -7       -6       -5         -4           -3      -2       -1
some_list   =     ["apple", "mango", "banana", "kiwi", "dragonfruit", "lemon", "lime", "orange"]
                        </code> </pre>
                    <br>
                    <p>The same indexing principe works for strings. String is sequence of characters. "Hello Fiona" is made
                        by "H", "e", "l",  "l",  "o",  " ", "F",  "i",  "o",  "n", "a". Therefore you can apply the same indexing and slicing methods.</p>
                    <p>Which indexing/slicing (without square brackets) we need to use to get <i>"Forum"</i> from string <i>"Active Learning Forum"</i>? </p>
                    <pre><code class="python">#propertie of ALF file
app_name = "Active Learning Forum"
comment = "The native application for Active Learning Forum."
type = "Shortcut"
size = "2.54 KB"
</code> </pre>
                    <p>If you think you answer is 100% correct, but not working, use "passcode13".</p>
                    <br><br><br>
                    <p>Example of answer format of index [1:4:-1]: 1:4:-1</p>
                    <br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
