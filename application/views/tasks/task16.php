<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class=" px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 16</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Correct! Be carefull mostly on single "=" in if statement. It can destroy whole code.</p>
                    <br>
                    <p>There is new #emergentproperty in Complex system of Minerva Students. Nobody can explain it.
                        Students got fascinated by Palindromes. The palindrome is a word, phrase, number, or other
                        sequence of characters which reads the same backward or forward. All Minerva students tries to
                        change their names in ALF to some Palindrome. This phenomenon is so big, that ALF needs time
                        effective algorithm to recognize the palindrome and deny name change. Help the ALF dev team detect palindromes. Time effective means, you
                        cannot use any loops. Use your knowledge about palindromes, indexing/slicing of strings and if
                        statements to detect if given student name is a palindrome. </p>
                    <p>If the given name is palindrome (example: racecar), your answer is "IsPalindrome", if not (example: analogy), your answer is "NotPalindrome". Please save your code for later review.</p>
                    <pre><code class="python">#check if this is a palindrome If the word is too long, Zoom-out the screen
given name="spiteditsleepsomerealspinsspiteditsleepsomerealspinsspiteditsleepsomerealspinsspiteditsleepsomerealspinsogrestatetrapgayevildeerawareedliveyagpartetatsergosnipslaeremospeelstidetipssnipslaeremospeelstidetipssnipslaeremospeelstidetipssnipslaeremospeelstidetips"
                        </code> </pre>

                    <br><br><br>
                    <br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
