<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 4</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Good Job! Your computation is right!</p>
                    <br>



                    <p>In Python you can work with different types of data. Your classmate is witting a list of available datatypes in Python 3. Which of these datatypes is NOT existing in Python:</p>
                    <p>Integer</p>
                    <p>Float</p>
                    <p>String</p>
                    <p>List</p>
                    <p>Tuple</p>
                    <p>Set</p>
                    <p>Turtle</p>
                    <p>Dictionary</p>
                    <p>Pandas</p>



                    <br><br><br><br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
