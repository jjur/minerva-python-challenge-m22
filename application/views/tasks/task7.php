<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 7</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Good Job! You found correct function.</p>
                    <br>

                    <p>It is often convenient to hold entire collections of data items. Python provides several
                        collection data
                        types that can hold items. The most important for us are lists. Python lists can be used to hold
                        any number
                        of data items of any data types. Lists are mutable, so we can easily insert items and remove
                        items whenever we want.</p>
                    <p>Your manager thinks you can be very successful programmer in the future. For now, he asks you to
                        create the list with your current course codes in alphabetical order (like SS150, AH51...) .</p>
                    <p>Fill the variable <i>available_courses</i> with list of your sorted courses. Run the code and use
                        the result as answer.</p>

                    <pre><code class="python">
available_courses =  #your list with content
for course in available_courses:
    print(course, sep="", end="")

</code> </pre>

                    <br><br><br><br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
