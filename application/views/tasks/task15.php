<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 15</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Correct! The PreClass Work was too long.</p>
                    <br>
                    <p>To make if statement working, there needs always be True or False. To get True or False, you can use logical operators.</p>
                    <p>Fell free to review this <a href="https://www.w3schools.com/python/python_conditions.asp"> article https://www.w3schools.com/python/python_conditions.asp</a> </p>
                    <p>Do a sum of line numbers, that contains If statements with invalid logical condition. </p>
                    <pre><code class="python">
a= 10              # line 1
b = 30             # line 2
if (a > b):        # line 3
    print("a>b")   # line 4
if (a => b):       # line 5
    print("a=>b")  # line 6
if (a > b):        # line 7
    print("a > b") # line 8
if (a <= b):       # line 9
    print("a<=b")  # line 10
if (a = b):        # line 11
    print("a = b") # line 12
if (a ! b):        # line 13
    print("a ! b") # line 14
if (a and b):      # line 15
    print("a and b")# line 16
if (a or b):       # line 17
    print("a or b")# line 18
                        </code> </pre>

                    <br><br><br>
                    <br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
