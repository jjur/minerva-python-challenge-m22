<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 13</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Good Job! You correctly identified correct slicing of given string.</p>
                    <br>
                    <p>Sometimes we are working with very large lists or strings and we cannot manualy count numbers of
                        elements. In this case we need to count number of characters in submited Pre-class Work.</p>
                    <p>Find and use function, which will calculate the number of elements in the list or characters in
                        the string. Apply this function to given string <i>preclass_work_part1</i></p>
                    <pre><code class="python">
preclass_work_part1 = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vulputate.
Nam ultrices, risus vitae rutrum maximus, erat quam placerat nulla, vitae tempus tellus neque
vitae tellus. Aenean sagittis suscipit justo, quis posuere quam vulputate ac. In ac arcu at
mauris vehicula efficitur. Nulla pulvinar eu ex eget consequat. Integer ut lacinia lectus, id
elementum tellus. In lobortis felis libero, nec fringilla ligula euismod a.
Integer at ullamcorper nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget
blandit quam, eu imperdiet ipsum. Ut eu rutrum enim, in ornare dolor. Nunc a fringilla ligula.
Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas consectetur vitae arcu
sit amet interdum. Praesent eleifend pretium erat. Mauris eget interdum magna, in tempus lectus.
Etiam sagittis fringilla libero et pellentesque. Duis imperdiet blandit sapien a elementum.
Nunc nisl lacus, euismod sed fringilla ac, pharetra et lacus. Suspendisse vel nulla justo. Nam
risus odio, eleifend interdum consequat eu, semper vel lacus. Morbi porta lacinia lacus, et
maximus nibh eleifend sed. Donec bibendum auctor enim non luctus. Pellentesque eget nunc nisi.
Fusce faucibus ultrices risus, ac mattis elit feugiat ut.
Morbi tempor nisi eget commodo bibendum. Quisque rhoncus ligula nec elit feugiat, rutrum mollis
leo fermentum. Quisque eget vehicula elit. Suspendisse vehicula ultrices congue. Suspendisse vel
eleifend dolor. Integer luctus ipsum turpis, sed auctor lorem feugiat at. Cras non dictum nulla.
Quisque quis sollicitudin neque, maximus maximus dolor. Donec sollicitudin dolor est, vitae
venenatis erat semper at. Integer eget orci non nulla egestas commodo et eu magna. Suspendisse
commodo, ligula sit amet commodo eleifend, nunc ante tempus nibh, nec egestas nibh ante eget
dolor. Integer eu sollicitudin elit, in luctus arcu. Donec diam sem, semper auctor ipsum nec,
interdum feugiat risus. Donec nec arcu molestie, viverra lorem ac, mollis diam. Integer
malesuada tellus eget odio pretium, quis pretium nulla euismod.
Sed ut accumsan justo. Sed id purus a felis pellentesque egestas. Aliquam gravida placerat
lectus nec viverra. Suspendisse potenti. Nunc aliquam tempus diam eu commodo. Aenean vehicula
est convallis metus molestie auctor. Nullam eu malesuada turpis, at pretium leo. Curabitur
dictum augue quam, eget tempus sapien suscipit ut.
Phasellus cursus mauris sit amet neque efficitur pharetra. Cras leo diam, tincidunt sit amet
justo ut, fermentum pellentesque velit. Vestibulum ante ipsum primis in faucibus orci luctus et
ultrices posuere cubilia Curae; Sed id eros urna. Duis egestas purus sed aliquam facilisis.
Aenean tempor odio at nulla vestibulum aliquet. Proin elit orci, imperdiet non neque id, cursus
ultricies nunc. Aliquam erat volutpat. Nullam scelerisque ipsum quis nulla feugiat dictum.
Vestibulum rutrum congue sapien, et condimentum urna porttitor eu. Nulla leo purus, rutrum sed
tristique ac, lobortis eget est.
Sed cursus, purus eu mattis aliquam, ante tellus consectetur ipsum, eu interdum lectus purus
pulvinar tellus. Fusce tristique pretium ipsum ac imperdiet. Maecenas vestibulum interdum metus,
eu sollicitudin arcu. Maecenas nisi erat, pretium gravida quam sed, placerat semper est.
Praesent ut ultricies arcu, nec venenatis magna. Sed pulvinar felis ac eleifend vehicula. Lorem
ipsum dolor sit amet, consectetur adipiscing elit. Curabitur varius ligula sed est rhoncus, quis
elementum velit aliquet. Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus. Nulla hendrerit leo in mattis laoreet. Suspendisse id ipsum purus. Sed vitae
pretium lectus.
Nunc a erat et eros luctus dapibus. Quisque vitae scelerisque diam. Morbi luctus maximus
sollicitudin. Mauris nec justo nisi. Nam venenatis vestibulum diam, sit amet lacinia leo semper
id. Fusce id dui orci. Donec nec nunc ac lacus mollis lobortis. Vestibulum in ultrices ipsum, et
egestas nibh.
Curabitur ex justo, malesuada vel massa et, tincidunt luctus ante. In odio leo, varius sed
bibendum dapibus, iaculis non massa. Quisque suscipit ultricies est et maximus. Ut sem ante,
accumsan consequat elit eu, sollicitudin consectetur lorem. In suscipit mollis est eu efficitur.
Aenean sit amet semper nisi. Maecenas malesuada vulputate semper. Orci varius natoque penatibus
et magnis dis parturient montes, nascetur ridiculus mus. Morbi tincidunt justo at metus
consectetur eleifend. Praesent at ligula pretium, laoreet velit a, blandit velit. Duis efficitur
quam ut consequat posuere. Nulla id erat non dui finibus condimentum sed sit amet leo. In
egestas tellus at nisl aliquet, pulvinar vehicula neque placerat. Duis nec efficitur sapien.
Suspendisse auctor commodo scelerisque. Duis velit lorem, euismod quis maximus sed, luctus
bibendum justo.
Sed vel viverra ex, sit amet iaculis lectus. Nam id nunc eros. Etiam felis erat, efficitur quis
massa at, viverra tincidunt lectus. Duis sodales urna quis lacus consequat pellentesque.
Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut
rhoncus commodo sem nec pellentesque. Maecenas facilisis est sed nibh ultricies, ac consequat
arcu maximus. Proin lorem odio, tincidunt sed nibh a, venenatis condimentum massa. Duis semper
massa in mauris varius congue. Sed congue eros id fringilla luctus. Nam aliquet est eget libero
malesuada, non consectetur libero ornare. Duis eros purus, elementum vitae gravida ut, mattis id
lectus. Sed at dapibus ex. Sed commodo nulla erat, nec vehicula dolor ullamcorper non. Donec ac
rhoncus justo. Duis vestibulum lacinia est, ac tempus purus faucibus quis."""
                        </code> </pre>

                    <br><br><br>
                    <p>Example of answer format of index [1:4:-1]: 1:4:-1</p>
                    <br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
