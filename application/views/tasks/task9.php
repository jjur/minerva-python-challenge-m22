<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 9</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Good Job! You correctly identified wrong commented lines.</p>
                    <br>


                    <p>We already know that list are collection of many elements. Sometimes we want to get an specific
                        element from the list. The list in the code contains enrolled students in course. The same list
                        as you can see in the right panel in ALF. </p>
                    <p>Use index to get the name of the professor. (The fist element in the list.) Which is the correct index for this case?</p>
                    <pre><code class="python">
enrolled_students = ["Prof. Terrana",
                    "Juraj Vasek",
                    "Austin Pérez del Castillo",
                    "Oluwakorede Akande",
                    "Li-Lian Ang",
                    "Iryna Bilohorka",
                    "Frederik Hardervig",
                    "Amenti Kenea",
                    "Barbara Machado",
                    "Anungoo Munkhsaikhan",
                    "Sam Scarfone",
                    "Viktoriia Stepanenko",
                    "Uyen Ton",
                    "Mahmud Un Nobe",
                    "Xiaohan (Julia) Wu"]

print("The professor in the course is:", enrolled_students)
</code> </pre>

                    <br><br><br>
                    <br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
