<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 8</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Good Job! You correctly created a list.</p>
                    <br>


                    <p>List have special methods(functions that applies to object before dot and do not return any
                        data), that are accessible by putting dot after name of the list, for example: <i>my_list.append("new
                            item")</i></p>
                    <p>Please check the documentation to explore the functions. This code was written in preliminary
                        assessment by one student. This code contains some useful list methods with comment. Which of
                        them has comments with incorrect meaning? Submit the names in alphabetical order. </p>
                    <pre><code class="python">
my_list_variable = ["Hi", "Ahoj", "Hallo", "Aloha", "Ola"]
my_list_variable.clear()       #Removes the last element from the list
my_list_variable.append("Cau") #Adds element to the beginning of the list
my_list_variable.sort()        #Randomly sorts the list
my_list_variable.pop()         #Removes the last element or element at given position
my_list_variable.count()       #Counts number of all elements in the list
</code> </pre>

                    <br><br><br>
                    <p>Example of answer format: AppleBlackCoffeeDayElephant</p>
                    <br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
