<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 5</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Good Job! You found the non-existing datatype.</p>
                    <br>


                    <p>Once we have some data types,the next thing we need are variables in which to store them. The
                        syntax for crating a variables(objectReferences) is: <i>variable_name = value</i>. The variable
                        needs to be created before accessing. You variable can have any name except Python keywords or
                        build-in functions. Your variable name cannot start by number or special character like _. </p>
                    <p>Review the list of keywords: <a href="https://www.w3schools.com/python/python_ref_keywords.asp">https://www.w3schools.com/python/python_ref_keywords.asp</a></p>
                    <p>and built-in functions: <a href="https://docs.python.org/3/library/functions.html">https://docs.python.org/3/library/functions.html</a></p>
                    <p>Review the code and identify incorrect variable names. As answer, put all incorrect variable names in alphabetical order from A to Z.</p>
                    <pre><code class="python">
a = 500
b = "Hello World"
c = 200
range = a - c
abs = ((c - a)**2)**0.5
loop = ["A", "B", "C"]
int = 100
while(True):
    loop.append("sum")
    if (range > int):
        input = "The range is too big!"
        return_me = False
        break
import = "plt"
print(input)
                        </code></pre>
                    <p>Example of answer format: AppleBlackCoffeeDayElephant</p>


                    <br><br><br><br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
