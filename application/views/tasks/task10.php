<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 10</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Good Job! You correctly identified correct index of professor. The lists are using the positions arguments starting from 0</p>
                    <pre><code class="python">
#index of element |   0        1        2        3          4            5       6        7
#index of element |  -8       -7       -6       -5         -4           -3      -2       -1
some_list   =     ["apple", "mango", "banana", "kiwi", "dragonfruit", "lemon", "lime", "orange"]
                        </code> </pre>
                    <br>
                    <p>Indexing or slicing is very powerfull tool. As position argument you can use: </p>
                    <p><b>single position: </b> [position], for example [4]</p>
                    <pre><code class="python">some_list[4] #is "dragonfruit"</code></pre>
                    <p><b>start and stop: </b> [start<b>:</b>stop], for example [5<b>:</b>7]</p>
                    <pre><code class="python">some_list[5:7] #is ["lemon", "lime"]</code></pre>
                    <p><b>start, stop, step: </b> [start<b>:</b>stop<b>:</b>step], for example [1<b>:</b>7<b>:</b>2]</p>
                    <pre><code class="python">some_list[1:7:2] #is ['mango', 'kiwi', 'lemon']</code></pre>

                    <p>Which indexing/slicing (without square brackets) we need to use to get ['orange', 'lemon', 'kiwi'] from some_list? </p>
<p>If you think you answer is 100% correct, but not working, use "passcode11".</p>

                    <br><br><br> <p>Example of answer format of index [1:4:-1]: 1:4:-1</p>
                    <br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
