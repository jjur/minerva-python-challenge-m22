<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 6</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Good Job! You found all incorrect variable names in the code of ALF</p>
                    <br>


                    <p>You know it very good. Every day you open the ALF, there is your personal greeting with quote or number of today´s classes. </p>
                    <p>Your classmate has written this code, but it is not working:</p>
                    <pre><code class="python">
name = "Juraj"
classes_today = 2
assignments_due = 0
line_1 = "Hello" + " " + name + "!"
line_2 = "You have " + classes_today + " and " + assignmnets_due + " due today."
</code> </pre>
                    <p>The variable line_1 created (assigned) correctly, but the next line fails.</p>
                    <p>Your MiMentor from M20 says you, that you cannot mix apples with oranges. And means datatypes.</p>
                    <p>To convert datatypes, there are a few simple functions (fell free to explore documentation if needed):</p>
                    <p>int(any_variable_to_convert)</p>
                    <p>float(any_variable_to_convert)</p>
                    <p>str(any_variable_to_convert)</p>
                    <p>list(any_variable_to_convert)</p>

                    <p>What is the name of function, which you can use to fix the code? (Submit name without brackets)</p>


                    <br><br><br><br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
