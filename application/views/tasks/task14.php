<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("tasks/header");
?>
<div class="clearfix bg-whiteish">
    <div class="container px2">
        <div class="mb3 mt3 border border-light rounded p3 border-box col-12 md-col-11 mx-auto">
            <h1 class="h0 serif darkest mt1 mb3 bold"><span class="boldest">Challenge 14</span></h1>
            <div class="flex mb3">
                <div class="">
                    <p class="h5">Wow! The number of characters was counted properly and precise. Keed going!</p>
                    <br>
                    <p>Now we need to decide if the preclass work is between allowed range. For any decision in coding,
                        we can use if statement. Until now, the code was executed line by line. If statement allows us
                        to execute the block of code, only in case the statement is true. Syntax is very simple:</p>
                    <pre><code class="python">
#Basic code, executed line by line
a = 50
b = 20
c = a + b
d = a // b
e = a + b + c + d
#if statement
if (c > 50):
    # 4 spaces intended code marks the lines of code, which will be executed only
    # if the conditional is true.
    print("The c > 50")
    print("Yes it is really bigger")
    print("I know you do not believe me, it is amazing!")
    print("Even, this line is executed when the conditional is true")
    print("How to exit this block of code?") #Just delete for spaces to get under previous code
print ("This outside the intended block, this is executed always")
                        </code> </pre>
                    <p>Which line will be printed? Use it as answer.</p>
                    <pre><code class="python">
preclass_work_part1_length = 5632
if (preclass_work_part1_length <600):
    print("PreclassWorkOK")
else:
    print("PreclassWorkTooLong")
                        </code> </pre>

                    <br><br><br>
                    <br><br>
                    <p><b>Submit answer as <?php echo base_url(); ?>/Challenge/A/[YourAnswer]</b></p>
                    <p>Example: <?php echo base_url(); ?>/Challenge/A/HelloWorld</p>

                    <br>
                    <p class="h5">During this challenge you can use Google, Python documentation or <a
                                href="http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf"> this book.</a></p>
                </div>
            </div>

        </div>
    </div>
</div>


<?php
$this->load->view("tasks/footer");
?>
