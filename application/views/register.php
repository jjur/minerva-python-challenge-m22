<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths mds-events-icons-ready icon-events-icons-ready icon-events-typefaces-linearicons-ready icon-events-typefaces-linearicons-normal-normal-ready" style="" data-useragent="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0" lang="en"><!--<![endif]--><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"><script type="text/javascript" src="<?php echo base_url(); ?>assets3/42086d9a90"></script><script src="<?php echo base_url(); ?>assets3/nr-1099.js"></script><script async="" src="<?php echo base_url(); ?>assets3/st.js"></script><script type="text/javascript" charset="UTF-8" async="" src="<?php echo base_url(); ?>assets3/cc.js"></script><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(3),u=e(4),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}catch(e){throw f.emit("fn-err",[arguments,this,e],t),e}finally{f.emit("fn-end",[c.now()],t)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){if(!o)return!1;if(e!==o)return!1;if(!n)return!0;if(!i)return!1;for(var t=i.split("."),r=n.split("."),a=0;a<r.length;a++)if(r[a]!==t[a])return!1;return!0}var o=null,i=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var u=navigator.userAgent,f=u.match(a);f&&u.indexOf("Chrome")===-1&&u.indexOf("Chromium")===-1&&(o="Safari",i=f[1])}n.exports={agent:o,version:i,match:r}},{}],3:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],4:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],5:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=v(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){h[e]=v(e).concat(n)}function m(e,n){var t=h[e];if(t)for(var r=0;r<t.length;r++)t[r]===n&&t.splice(r,1)}function v(e){return h[e]||[]}function g(e){return p[e]=p[e]||o(t)}function w(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var h={},y={},b={on:l,addEventListener:l,removeEventListener:m,emit:t,get:g,listeners:v,context:n,buffer:w,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(3),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!E++){var e=x.info=NREUM.info,n=l.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+x.offset],null,"api");var t=l.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===l.readyState&&i()}function i(){f("mark",["domContent",a()+x.offset],null,"api")}function a(){return O.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-x.offset}var u=(new Date).getTime(),f=e("handle"),c=e(3),s=e("ee"),p=e(2),d=window,l=d.document,m="addEventListener",v="attachEvent",g=d.XMLHttpRequest,w=g&&g.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:g,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var h=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1099.min.js"},b=g&&w&&w[m]&&!/CriOS/.test(navigator.userAgent),x=n.exports={offset:u,now:a,origin:h,features:{},xhrWrappable:b,userAgent:p};e(1),l[m]?(l[m]("DOMContentLoaded",i,!1),d[m]("load",r,!1)):(l[v]("onreadystatechange",o),d[v]("onload",r)),f("mark",["firstbyte",u],null,"api");var E=0,O=e(5)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":1,"licenseKey":"42086d9a90","agent":"","transactionName":"YgRRYkoADBVTWxYPVltOdUNWAhYPXVZNB0lFDVpVWRULCVwWFA9cQhIdQVEbAxRWAhUPQ1QTV2lLFgsSUVA=","applicationID":"23601687","errorBeacon":"bam.nr-data.net","applicationTime":1046}</script>
    <title>Apply | Minerva Schools</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta id="current_app_term" content="Fall 2019">
    <meta name="disable_autosave" content="false">

    <link rel="apple-touch-icon" sizes="180x180" href="https://d33z52hfhvk3mr.cloudfront.net/public/apple-touch-icon.png?_v=060d84e">
    <link rel="icon" type="image/png" href="https://d33z52hfhvk3mr.cloudfront.net/public/favicon-32x32.png?_v=060d84e" sizes="32x32">
    <link rel="icon" type="image/png" href="https://d33z52hfhvk3mr.cloudfront.net/public/favicon-16x16.png?_v=060d84e" sizes="16x16">
    <link rel="manifest" href="https://d33z52hfhvk3mr.cloudfront.net/public/manifest.json?_v=060d84e">
    <link rel="mask-icon" href="https://d33z52hfhvk3mr.cloudfront.net/public/safari-pinned-tab.svg?_v=060d84e" color="#111111">
    <link rel="shortcut icon" href="https://d33z52hfhvk3mr.cloudfront.net/public/favicon.ico?_v=060d84e">
    <meta name="apple-mobile-web-app-title" content="Minerva">
    <meta name="application-name" content="Minerva">
    <meta name="theme-color" content="#202020">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets3/394DEFEB16E7CC939.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets3/c9f3e6b3-62a4-4aca-9909-a0b00b6edcb5.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets3/basscss.css">

    <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/bcoogmaokmoikhfiodnlalilblkojjld">
    <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/jioaiecngdmdgcnmjiappopamjjacjeg">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets3/jquery-ui_002.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets3/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets3/chosen.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets3/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets3/featherlight.css">
    <style>
        .featherlight .featherlight-content {
            padding: 0;
            border-bottom: none;
            max-width: 40em;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets3/colors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets3/forms.css">
    <script src="<?php echo base_url(); ?>assets3/fc434192.js"></script><link rel="stylesheet" href="<?php echo base_url(); ?>assets3/woff2_002.css" media="all">


    <!-- include on top (bad practice) because if we put them at the bottom
         we can't show the library imports in the different sections -->
    <!-- TODO: Bundle all the stuff for the forms together -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets3/core-bb20f0b87478a45a2a78fc3b40529278.js"></script>

    <script>
        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
    </script>

    <script type="text/javascript">
        (function(){
            var FLAGS = {
                    'access_gty_2016': true,'adm_dont_autoplay_intro_video': false,'admissions_open': true,'allow_new_applications_for_current_app_term': true,'cookiebot': true,'css_profile_open': true,'debugging_mode': false,'directed_admission_flow_test': false,'enable_assignment_queue': true,'enable_financial_aid_applications_for_current_students': false,'enable_financial_aid_applications_for_incoming_undergrads': true,'enable_financial_aid_applications_for_masters_students': false,'enable_payments_during_maintenance_mode': true,'grade_scales': false,'gty_2015_show_prepare_page': true,'gty_2015_skip_date_check': true,'include_intercom_snippet': false,'load_ve_prompts_from_cloudfront': true,'masters_admissions_open': true,'oauth': false,'proxy_cms_assets': true,'qm_proxy': true,'show_2016_prepare_page': true,'show_creativity_practice': false,'show_freshman_prepare_page': false,'skip_proctor': false,'summer_admission_center_testing': false,'summer_masters_admissions_testing': true,'transcript_grading_preview': false,'use_cloudfront_proctor': true
                },
                SWITCHES = {
                    'enable_sync_app_to_salesforce': true,'salesforce_sweeper_use_batch_api': true,'send_prepare_emails': false,'kinetophone_formatter_delivers_cloudfront_asset_urls': true,'automatically_issue_prepare_modules': true,'automate_sevis_batching': false
                },
                SAMPLES = {

                };
            window.waffle = {
                "flag_is_active": function waffle_flag(flag_name) {

                    return !!FLAGS[flag_name];
                },
                "switch_is_active": function waffle_switch(switch_name) {

                    return !!SWITCHES[switch_name];
                },
                "sample_is_active": function waffle_sample(sample_name) {

                    return !!SAMPLES[sample_name];
                },
                "FLAGS": FLAGS,
                "SWITCHES": SWITCHES,
                "SAMPLES": SAMPLES
            };
        })();

    </script>
    <script type="text/javascript">
        window.minerva = window.minerva || {};
        window.minerva.config = {
            ASSET_BASE_URL: "//d33z52hfhvk3mr.cloudfront.net/public/",
            ENVIRONMENT: "production",
            GIT_HASH: "060d84e",
            DEBUG: false,
            API_ROOT: "https://www.minerva.kgi.edu/api/v1/",
        };
    </script>

    <script id="Cookiebot" src="<?php echo base_url(); ?>assets3/uc.js" data-cbid="e59773c6-701e-4f1d-896f-a5359f4f4562" type="text/javascript" async="">
    </script>
    <!-- LinkedIn tracking -->

    <script type="text/plain" data-cookieconsent="marketing">
  _linkedin_data_partner_id = "229769";
  (function(){var s = document.getElementsByTagName("script")[0];
    var b = document.createElement("script");
    b.type = "text/javascript";b.async = true;
    b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
    s.parentNode.insertBefore(b, s);})();
</script>
    <noscript>
        <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=229769&fmt=gif" />
    </noscript>  <script>
        window.intercomSettings = {};
        window.intercomSettings['app_id'] = "y30r4n6w";
    </script>

    <script type="text/plain" data-cookieconsent="preferences">
      (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/y30r4n6w?v=force_refresh';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}})()



    setTimeout(function() {
      if (document.location.href.match(/postlogout=1/)) {
        Intercom('shutdown');
        setTimeout(function() {
            Intercom('boot', window.intercomSettings);
            setTimeout(function() {
                Intercom('show');
            }, 3000);
        }, 3000);
      }
    }, 3000);
  </script>
    <script type="text/javascript">
        var person = {
            username: ('; ' + document.cookie).split('; minerva_id=').pop().split(';')[0]
        };

        window._rollbarConfig = {
            accessToken: "eba9e8afdb504201b198aafe24e084b7",
            captureUncaught: false,
            captureUnhandledRejections: true,
            payload: {
                "environment": "production",
                "sha": "060d84e",
            },
            person: person,
        };
        // Rollbar Snippet
        !function(r){function e(n){if(o[n])return o[n].exports;var t=o[n]={exports:{},id:n,loaded:!1};return r[n].call(t.exports,t,t.exports,e),t.loaded=!0,t.exports}var o={};return e.m=r,e.c=o,e.p="",e(0)}([function(r,e,o){"use strict";var n=o(1),t=o(4);_rollbarConfig=_rollbarConfig||{},_rollbarConfig.rollbarJsUrl=_rollbarConfig.rollbarJsUrl||"https://cdnjs.cloudflare.com/ajax/libs/rollbar.js/2.4.4/rollbar.min.js",_rollbarConfig.async=void 0===_rollbarConfig.async||_rollbarConfig.async;var a=n.setupShim(window,_rollbarConfig),l=t(_rollbarConfig);window.rollbar=n.Rollbar,a.loadFull(window,document,!_rollbarConfig.async,_rollbarConfig,l)},function(r,e,o){"use strict";function n(r){return function(){try{return r.apply(this,arguments)}catch(r){try{console.error("[Rollbar]: Internal error",r)}catch(r){}}}}function t(r,e){this.options=r,this._rollbarOldOnError=null;var o=s++;this.shimId=function(){return o},"undefined"!=typeof window&&window._rollbarShims&&(window._rollbarShims[o]={handler:e,messages:[]})}function a(r,e){if(r){var o=e.globalAlias||"Rollbar";if("object"==typeof r[o])return r[o];r._rollbarShims={},r._rollbarWrappedError=null;var t=new p(e);return n(function(){e.captureUncaught&&(t._rollbarOldOnError=r.onerror,i.captureUncaughtExceptions(r,t,!0),i.wrapGlobals(r,t,!0)),e.captureUnhandledRejections&&i.captureUnhandledRejections(r,t,!0);var n=e.autoInstrument;return e.enabled!==!1&&(void 0===n||n===!0||"object"==typeof n&&n.network)&&r.addEventListener&&(r.addEventListener("load",t.captureLoad.bind(t)),r.addEventListener("DOMContentLoaded",t.captureDomContentLoaded.bind(t))),r[o]=t,t})()}}function l(r){return n(function(){var e=this,o=Array.prototype.slice.call(arguments,0),n={shim:e,method:r,args:o,ts:new Date};window._rollbarShims[this.shimId()].messages.push(n)})}var i=o(2),s=0,d=o(3),c=function(r,e){return new t(r,e)},p=function(r){return new d(c,r)};t.prototype.loadFull=function(r,e,o,t,a){var l=function(){var e;if(void 0===r._rollbarDidLoad){e=new Error("rollbar.js did not load");for(var o,n,t,l,i=0;o=r._rollbarShims[i++];)for(o=o.messages||[];n=o.shift();)for(t=n.args||[],i=0;i<t.length;++i)if(l=t[i],"function"==typeof l){l(e);break}}"function"==typeof a&&a(e)},i=!1,s=e.createElement("script"),d=e.getElementsByTagName("script")[0],c=d.parentNode;s.crossOrigin="",s.src=t.rollbarJsUrl,o||(s.async=!0),s.onload=s.onreadystatechange=n(function(){if(!(i||this.readyState&&"loaded"!==this.readyState&&"complete"!==this.readyState)){s.onload=s.onreadystatechange=null;try{c.removeChild(s)}catch(r){}i=!0,l()}}),c.insertBefore(s,d)},t.prototype.wrap=function(r,e,o){try{var n;if(n="function"==typeof e?e:function(){return e||{}},"function"!=typeof r)return r;if(r._isWrap)return r;if(!r._rollbar_wrapped&&(r._rollbar_wrapped=function(){o&&"function"==typeof o&&o.apply(this,arguments);try{return r.apply(this,arguments)}catch(o){var e=o;throw e&&("string"==typeof e&&(e=new String(e)),e._rollbarContext=n()||{},e._rollbarContext._wrappedSource=r.toString(),window._rollbarWrappedError=e),e}},r._rollbar_wrapped._isWrap=!0,r.hasOwnProperty))for(var t in r)r.hasOwnProperty(t)&&(r._rollbar_wrapped[t]=r[t]);return r._rollbar_wrapped}catch(e){return r}};for(var u="log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection,captureEvent,captureDomContentLoaded,captureLoad".split(","),f=0;f<u.length;++f)t.prototype[u[f]]=l(u[f]);r.exports={setupShim:a,Rollbar:p}},function(r,e){"use strict";function o(r,e,o){if(r){var t;if("function"==typeof e._rollbarOldOnError)t=e._rollbarOldOnError;else if(r.onerror){for(t=r.onerror;t._rollbarOldOnError;)t=t._rollbarOldOnError;e._rollbarOldOnError=t}var a=function(){var o=Array.prototype.slice.call(arguments,0);n(r,e,t,o)};o&&(a._rollbarOldOnError=t),r.onerror=a}}function n(r,e,o,n){r._rollbarWrappedError&&(n[4]||(n[4]=r._rollbarWrappedError),n[5]||(n[5]=r._rollbarWrappedError._rollbarContext),r._rollbarWrappedError=null),e.handleUncaughtException.apply(e,n),o&&o.apply(r,n)}function t(r,e,o){if(r){"function"==typeof r._rollbarURH&&r._rollbarURH.belongsToShim&&r.removeEventListener("unhandledrejection",r._rollbarURH);var n=function(r){var o,n,t;try{o=r.reason}catch(r){o=void 0}try{n=r.promise}catch(r){n="[unhandledrejection] error getting `promise` from event"}try{t=r.detail,!o&&t&&(o=t.reason,n=t.promise)}catch(r){t="[unhandledrejection] error getting `detail` from event"}o||(o="[unhandledrejection] error getting `reason` from event"),e&&e.handleUnhandledRejection&&e.handleUnhandledRejection(o,n)};n.belongsToShim=o,r._rollbarURH=n,r.addEventListener("unhandledrejection",n)}}function a(r,e,o){if(r){var n,t,a="EventTarget,Window,Node,ApplicationCache,AudioTrackList,ChannelMergerNode,CryptoOperation,EventSource,FileReader,HTMLUnknownElement,IDBDatabase,IDBRequest,IDBTransaction,KeyOperation,MediaController,MessagePort,ModalWindow,Notification,SVGElementInstance,Screen,TextTrack,TextTrackCue,TextTrackList,WebSocket,WebSocketWorker,Worker,XMLHttpRequest,XMLHttpRequestEventTarget,XMLHttpRequestUpload".split(",");for(n=0;n<a.length;++n)t=a[n],r[t]&&r[t].prototype&&l(e,r[t].prototype,o)}}function l(r,e,o){if(e.hasOwnProperty&&e.hasOwnProperty("addEventListener")){for(var n=e.addEventListener;n._rollbarOldAdd&&n.belongsToShim;)n=n._rollbarOldAdd;var t=function(e,o,t){n.call(this,e,r.wrap(o),t)};t._rollbarOldAdd=n,t.belongsToShim=o,e.addEventListener=t;for(var a=e.removeEventListener;a._rollbarOldRemove&&a.belongsToShim;)a=a._rollbarOldRemove;var l=function(r,e,o){a.call(this,r,e&&e._rollbar_wrapped||e,o)};l._rollbarOldRemove=a,l.belongsToShim=o,e.removeEventListener=l}}r.exports={captureUncaughtExceptions:o,captureUnhandledRejections:t,wrapGlobals:a}},function(r,e){"use strict";function o(r,e){this.impl=r(e,this),this.options=e,n(o.prototype)}function n(r){for(var e=function(r){return function(){var e=Array.prototype.slice.call(arguments,0);if(this.impl[r])return this.impl[r].apply(this.impl,e)}},o="log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection,_createItem,wrap,loadFull,shimId,captureEvent,captureDomContentLoaded,captureLoad".split(","),n=0;n<o.length;n++)r[o[n]]=e(o[n])}o.prototype._swapAndProcessMessages=function(r,e){this.impl=r(this.options);for(var o,n,t;o=e.shift();)n=o.method,t=o.args,this[n]&&"function"==typeof this[n]&&("captureDomContentLoaded"===n||"captureLoad"===n?this[n].apply(this,[t[0],o.ts]):this[n].apply(this,t));return this},r.exports=o},function(r,e){"use strict";r.exports=function(r){return function(e){if(!e&&!window._rollbarInitialized){r=r||{};for(var o,n,t=r.globalAlias||"Rollbar",a=window.rollbar,l=function(r){return new a(r)},i=0;o=window._rollbarShims[i++];)n||(n=o.handler),o.handler._swapAndProcessMessages(l,o.messages);window[t]=n,window._rollbarInitialized=!0}}}}]);
        // End Rollbar Snippet
    </script>  <!-- GA tracking -->

    <script type="text/plain" data-cookieconsent="statistics">
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-33547340-2', 'kgi.edu');

      ga('require', 'GTM-TVGTD2M');
      ga('send', 'pageview');
  </script>
    <!-- end GA tracking -->

    <script type="text/javascript">
        !function(f,b,e,v,n,t,s){
            if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];
        }(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');
    </script>

    <script type="text/plain" data-cookieconsent="marketing">
    !function(f,b,e,v,n,t,s){
    t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)
    }(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '765184353562105');
    fbq('track', 'PageView');
  </script>

    <noscript><img height="1" width="1" alt="" style="display:none"
                   src="https://www.facebook.com/tr?id=765184353562105&ev=PageView&noscript=1"
        /></noscript>
    <!-- Mixpanel tracking -->
    <script type="text/plain" data-cookieconsent="statistics">
    (function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
    for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);

    mixpanel.init("9f072556220223b79990874505b39595", {
      "loaded": function() {
        if (document.location.href.match(/postlogout=1/)) {
          mixpanel.cookie.clear();
        }
      }
    });

  </script>
    <!-- Begin Inspectlet Embed Code -->
    <script id="inspectletjs" type="text/plain" data-cookieconsent="statistics">
    window.__insp = window.__insp || [];
    __insp.push(['wid', 799128071]);
    (function() {
      function ldinsp(){if(typeof window.__inspld != "undefined") return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
      setTimeout(ldinsp, 500); document.readyState != "complete" ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp();
    })();
  </script>
    <!-- End Inspectlet Embed Code -->

<div id="site-wrapper"><div id="site-canvas">


        <!-- NAVIGATION -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets3/animate.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets3/hover-min.css">

        <section class="bg-black">
            <div class="container px2">
                <div class="clearfix mxn1 flex flex-center">
                    <div class="col col-12 md-col-4 py2 px1">
                        <a href="https://www.minerva.kgi.edu/" class="no-hover">
                            <img src="<?php echo base_url(); ?>assets3/logo-minerva-schools-white-transparent.png" style="height:48px;top:4px" class="relative" alt="Minerva Logo">
                        </a>
                    </div>
                    <div class="md-show col col-6 sm-col-8 right-align">
                        <div class="inline-block px1 md-px3">
                            <a href="https://www.minerva.kgi.edu/application/login/" class="bold whiteish text-decoration-none hover-orange">Login</a>
                        </div>
                        <div class="inline-block px1">
                            <a href="https://www.minerva.kgi.edu/contact" class="bold whiteish text-decoration-none hover-orange">Contact</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="md-show bg-dark py2 border-bottom border-light">
            <div class="container mxn1">
                <div class="clearfix">
                    <div class="md-col md-col-6">
                        <p class="no-leading m0 p0 bold text-decoration-none white">Undergraduate Program Python Challenge
                        </p></div>
                </div>
            </div>
        </section>

        <script src="<?php echo base_url(); ?>assets3/application_header.js" type="text/javascript"></script>
        <!-- END NAVIGATION -->

        <!-- outside of container -->

        <div class="clearfix bg-whiteish md-bg-sidebar-gradient">
            <div class="container px2">
                <!--left content -->
                <div class="flex">
                    <div class="flex-auto py3 md-col-7 sm-col sm-col-12">
                        <header class="category">
                            <h1 class="serif regular mb0">Start Your Application</h1>
                            <p class="serif mt1">Complete Part One to view full admissions requirements in the Admission Center</p>
                        </header>


                        <form method="POST" id="application-form" class="application-form step-name-1 ws-validate" autocomplete="off" data-leave-message="You will lose all data on this step, are you sure you want to leave?" data-max-forms="" data-has-instance="0" data-step-name="1" data-step-number="1">

                            <input type="hidden" name="csrfmiddlewaretoken" value="ASsFQFtgDWFgI0E4v9qcvE3fL8wNQFC8T1rHTLAkwmFv2mhwdrQRfb4uUxf95eWS">


                            <div class="mt3">
                                <div class="clearfix">
                                    <div class="col col-12 md-col-6">
                                        <div class="clearfix">
                                            <div class="col col-12 mb1">
                                                <label for="id_first_name">First name *</label>
                                            </div>

                                            <div class="col col-12">
                                                <div class="mr2">
                                                    <input id="id_first_name" name="first_name" type="text" placeholder="Your first name" maxlength="100" required="">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col col-12 md-col-6">
                                        <div class="clearfix">
                                            <div class="col col-12 mb1">
                                                <label for="id_last_name">Last or family name *</label>
                                            </div>

                                            <div class="col col-12">
                                                <div class="mr2">
                                                    <input id="id_last_name" name="last_name" type="text" placeholder="Your last or family name" maxlength="100" required="">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="serif mt3">
                                If you have subscribed to receive other communications from Minerva,
                                please use the same email address here. This will help to ensure you
                                receive communications relevant to where you are in completing the
                                admissions process.
                            </div>
                            <div class="mt3">
                                <div class="clearfix">
                                    <div class="col col-12 md-col-6">
                                        <div class="clearfix">
                                            <div class="col col-12 mb1">
                                                <label for="id_email">Email address *</label>
                                            </div>

                                            <div class="col col-12">
                                                <div class="mr2">
                                                    <input id="id_email" name="email" type="email" placeholder="emailname@email.com" maxlength="255" required="">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col col-12 md-col-6">
                                        <div class="clearfix">
                                            <div class="col col-12 mb1">
                                                <label for="id_confirm_email">Confirm email address *</label>
                                            </div>

                                            <div class="col col-12">
                                                <div class="mr2">
                                                    <input id="id_confirm_email" name="confirm_email" type="email" placeholder="yourname@email.com" required="">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="mt3">
                                <div class="clearfix">
                                    <div class="col col-12 md-col-6">
                                        <div class="clearfix mr2">
                                            <div class="col col-12">
                                                <div class="col col-12 mb1">
                                                    <label for="id_birthdate">Date of birth *</label>
                                                </div>

                                            </div>
                                            <div id="id_birthdate_container" class=" col col-12 js-minerva-form-split-date">
                                                <input name="birthdate" id="id_birthdate" type="hidden" value="" required="">
                                                <div class="col col-4">
                                                    <div class="mr1">
                                                        <input type="number" min="1" max="31" placeholder="Day" class="m0 js-day" required="">
                                                    </div>
                                                </div>
                                                <div class="col col-4">
                                                    <div class="mr1">
                                                        <select class="m0 js-month">
                                                            <option disabled="disabled" selected="selected" class="display-none">Month</option>
                                                            <option value="1">January</option>
                                                            <option value="2">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">August</option>
                                                            <option value="9">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col col-4">
                                                    <input type="number" placeholder="Year" min="1900" max="2020" class="m0 js-year" required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-12 md-col-6">
                                        <div class="js-minerva-form-autocomplete">
                                            <script type="application/json">
                                                ["Afghanistan", "Anguilla", "Aland Islands", "Albania", "Algeria", "Andorra", "Angola", "Antarctica", "Antigua & Deps", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bonaire, Saint Eustatius and Saba", "Bosnia Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina", "Burundi", "Cambodia", "Cayman Islands", "Cameroon", "Canada", "Cape Verde", "Central African Rep", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo (Democratic Rep)", "Cook Islands", "Costa Rica", "Ivory Coast", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Faroe Islands", "Falkland Islands (Malvinas)", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Heard Island And Mcdonald Islands", "Hong Kong", "Haiti", "Vatican City", "Honduras", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland (Republic)", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea North", "South Korea", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar, Burma", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niue", "Niger", "Nigeria", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "West Bank / Palestinian Territory", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "St Kitts & Nevis", "St Lucia", "Saint Helena", "Saint Martin", "Saint Pierre And Miquelon", "Saint Vincent And The Grenadines", "Saint-Barthelemy", "Saint-Martin (French Part)", "Samoa", "San Marino", "Sao Tome & Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten (Dutch part)", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "South Georgia And The South Sandwich Islands", "South Sudan", "Suriname", "Svalbard And Jan Mayen", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "East Timor", "Togo", "Tokelau", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks And Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Wallis And Futuna", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"]
                                            </script>
                                            <div class="clearfix">
                                                <div class="col col-12 mb1">
                                                    <label for="id_country">Country of residence (2018-2019) *</label>
                                                </div>

                                                <div class="col col-12">
                                                    <div class="mr2">
                                                        <input id="id_country" name="country" type="text" placeholder="Type your country name" required="" class="ui-autocomplete-input" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="mt3 mxn1">
                                <div class="col col-12 mt1 mb3">
                                    <div class="clearfix js-has_indicated_high_interest_in_attending">
                                        <div>
                                            <div class="skinned-checkbox left border rounded inline-block bg-whitest border-lightest enabled pointer" style="position: relative;"><input type="checkbox" id="id_has_indicated_high_interest_in_attending" name="has_indicated_high_interest_in_attending" class="js-minerva-form-checkbox" style="position: absolute; opacity: 0;"><span>✓</span><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                            <label for="id_has_indicated_high_interest_in_attending" class="ml3">Minerva is my first choice and I want to learn about Binding Enrollment.</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="mt3">
                                <div class="clearfix">
                                    <div class="col col-12 mb1">
                                        <label for="id_source">How did you learn about Minerva? *</label>
                                    </div>

                                    <div class="col col-12">
                                        <div class="mr2">
                                            <select id="id_source" name="source">
                                                <option value="" selected="selected"></option>
                                                <option value="School Visit">School Visit</option>
                                                <option value="Events (fair/conferences)">Events (fair/conferences)</option>
                                                <option value="Email">Email</option>
                                                <option value="Press (online and traditional)">Press (online and traditional)</option>
                                                <option value="Social Media">Social Media</option>
                                                <option value="Advertisement">Advertisement</option>
                                                <option value="Web Search">Web Search</option>
                                                <option value="Club/Organization/Group you are affiliated with (e.g., Olympiads, Model UN)">Club/Organization/Group you are affiliated with (e.g., Olympiads, Model UN)</option>
                                                <option value="School Official (teacher, counselor, etc)">School Official (teacher, counselor, etc)</option>
                                                <option value="Minerva Student">Minerva Student</option>
                                                <option value="Friend/Family">Friend/Family</option>
                                                <option value="Minerva Applicant">Minerva Applicant</option>
                                                <option value="Local Minerva Representative">Local Minerva Representative</option>
                                                <option value="Other">Other</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>








                            </div>
                            <div class="mt0 source-other" style="display: none;">
                                <div class="clearfix">
                                    <div class="col col-12 mb1">
                                        <label for="id_source_if_other">Please specify</label>
                                    </div>

                                    <div class="col col-12">
                                        <div class="mr2">
                                            <input id="id_source_if_other" name="source_if_other" type="text" maxlength="255">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="mt3">
                                <div class="clearfix">
                                    <div class="col col-12 mb1">
                                        <label for="id_spoken_languages">List all languages that you speak fluently *</label>
                                    </div>

                                    <div class="col col-12">
                                        <div class="mr2">
                                            <input id="id_spoken_languages" name="spoken_languages" type="text" placeholder="French, Chinese, English" required="">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="mt3 js-was-previously-admitted display-none">
                                <p class="regular p2 bg-300 border border-400 text-600">Welcome back. We look forward to reviewing your new application this year.</p>
                                <div class="clearfix">
                                    <div class="col col-12">
                                        <div class="col col-12 mb1">
                                            <label for="id_was_previously_admitted_0">Were you previously admitted to Minerva? *</label>
                                        </div>

                                    </div>
                                    <div class="col col-12">
                                        <div id="id_was_previously_admitted_0">
                                            <div class="inline-block mt1 mr1">
                                                <div class="skinned-radio rounded inline-block px2 py1 border border-lightest bg-eggshell mid enabled pointer" style="position: relative;"><input type="radio" name="was_previously_admitted" value="True" class="js-minerva-form-radio" data-text="Yes" style="position: absolute; opacity: 0;">Yes<ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                            </div>
                                            <div class="inline-block mt1 mr1">
                                                <div class="skinned-radio rounded inline-block px2 py1 border border-lightest bg-eggshell mid enabled pointer" style="position: relative;"><input type="radio" name="was_previously_admitted" value="False" class="js-minerva-form-radio" data-text="No" style="position: absolute; opacity: 0;">No<ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="serif mt3 mxn1">
                                <p class="italic">
                                    We rely on electronic communications for a number of our
                                    admissions-related processes. As such, by beginning your application to
                                    Minerva, you are opting in to receive these necessary email
                                    notifications, which may include but are not limited to; information
                                    about best practices for the admissions process, deadline reminders,
                                    outstanding or incomplete materials in your application.
                                </p>
                            </div>
                            <div class="mt1 mxn1">
                                <div class="clearfix js-agree">
                                    <div>
                                        <div class="skinned-checkbox left border rounded inline-block bg-whitest border-lightest enabled pointer" style="position: relative;"><input type="checkbox" id="id_agree" name="agree" class="js-minerva-form-checkbox" required="" style="position: absolute; opacity: 0;"><span>✓</span><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                        <label for="id_agree" class="ml3">I have read and agree to the Minerva Schools at KGI <a href="https://www.minerva.kgi.edu/terms/" target="_blank" class="text-accent-mid">Terms of Use</a> and <a href="https://www.minerva.kgi.edu/privacy/" target="_blank" class="text-accent-mid">Privacy Policy</a></label>
                                    </div>
                                </div>

                            </div>


                            <div class="mt4">

                                <div class="mr2 inline">
                                    <div class="clearfix">
                                        <div class="md-col md-col-6 undo-collapse"></div>
                                        <div class="md-col sm-col-3 px1 undo-collapse">
                                        </div>
                                        <div class="sm-col md-col-3 px1">
                                            <a name="submit" href="<?php echo base_url(); ?>/Challenge/" class="full-width center button bg-warning white regular mr2">Start Python Challenge</a>
                                           <br><br>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div> <!--end of left content -->


                    <div class="md-col md-col-1 md-show undo-collapse">
                    </div>

                    <div class="md-show ml2 md-col md-col-4 px3 py4 bg-eggshell serif clearfix">
                        <div class="border-bottom border-mid mb4">
                            <div class="inline-block">
                                <div class="mb1 sans-serif darkest h4 md-flex flex-center">
                                    <div class="mr2" style="width:32px;line-height:1.0;">
                                        <h6 class="m0"><span class="text-smaller">
            <span class="icon-stack icon-2x">
              <i class="icon icon-circle text-warning icon-stack-2x"></i>
              <i class="icon icon-arrow-right tan icon-stack-1x"></i>
            </span>
          </span></h6>
                                    </div>
                                    <div class="bold">Start Your Application
                                    </div>
                                </div>
                                <div class="mb1 sans-serif darkest h4 md-flex flex-center">
                                    <div class="mr2" style="width:32px;line-height:1.0;">
                                        <h6 class="m0"><span class="text-smaller">
            <span class="icon-stack icon-2x">
              <i class="icon icon-circle tan icon-stack-2x"></i>
              <i class="icon icon-check-thin eggshell icon-stack-1x"></i>
            </span>
          </span></h6>
                                    </div>
                                    <div class="bold">Access Admission Center
                                    </div>
                                </div>
                                <div class="mb1 sans-serif darkest h4 md-flex flex-center">
                                    <div class="mr2" style="width:32px;line-height:1.0;">
                                        <h6 class="m0"><span class="text-smaller">
            <span class="icon-stack icon-2x">
              <i class="icon icon-circle tan icon-stack-2x"></i>
              <i class="icon icon-check-thin eggshell icon-stack-1x"></i>
            </span>
          </span></h6>
                                    </div>
                                    <div class="bold">Apply to Minerva
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- show if promoting common app.  Remove for Extended Deadline -->
                        <!-- <article>
                          <div class="clearfix mb4">
                            <p class="h5"><strong>Note:</strong> Should you choose to apply via the Common Application, you will still be required to complete all parts of Minerva's admissions process.</p>
                            <a class="h5 button-link bold text-decoration-underline" href="http://www.commonapp.org/school/minerva-schools-kgi">commonapp.org/school/minerva-schools-kgi</a>
                            <hr class="mt3 mb3">
                          </div>
                        </article> -->
                        <article>
                        </article>
                        <!-- replace with next year's dates
                        <article>
                          <p class="h5"><strong>Early Action:</strong> due November 01, 2018. Candidates notified by December 15, 2018.</p>

                          <p class="h5"><strong>Regular Decision I:</strong> due January 15, 2019. Candidates notified by March 15, 2019.</p>

                          <p class="h5"><strong>Regular Decision II:</strong> due March 15, 2019. Candidates notified by April 15, 2019.</p>
                          <p class="h5"><strong>Extended Deadline:</strong> due April 16, 2018. Candidates notified by April 26, 2018.<br/>*All applicants must apply Binding Enrollment</p>
                          <hr class="mt3 mb3">
                        </article>-->
                    </div>
                </div>
            </div>
        </div>
        <!-- end outside of container -->

        <!-- Main -->
        <div class="clearfix bg-whiteish">
            <div class="container px2">
            </div>
        </div>
        <!-- End Main -->

        <!-- FOOTER -->
        <div class="border-box sticky-footer-buffer minimal"></div>

        <footer class="sticky minimal py2 bg-darkest absolute bottom-0 full-width border-box">

            <div class="container px2">
                <div class="clearfix mxn1 mt1 md-flex flex-center">
                    <div class="col col-12 md-col-4 px1 lightest mb2 md-mb0">
                        <div class="h6 thin">©2017 Minerva Project, Inc. All Rights Reserved.</div>
                    </div>
                    <div class="col col-12 md-col-4 px1 lightest mb2 md-mb0">
                        <div class="clearfix">
                            <div class="col col-6 h6 thin"><a href="https://www.minerva.kgi.edu/terms" class="lightest">Terms &amp; Conditions</a></div>
                            <div class="col col-6 h6 thin"><a href="https://www.minerva.kgi.edu/privacy" class="lightest">Privacy Policy</a></div>
                        </div>
                    </div>
                    <div class="col col-12 md-col-4 px1 lightest">
                        <div class="right-align clearfix">
                            <div class="col-right col-3">
                                <img src="<?php echo base_url(); ?>assets3/logo-KGI.svg" alt="KGI logo" style="width:80px">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </footer>      <!-- END FOOTER -->


        <!--[if lt IE9]>
        <script src="/public/js/html5.js"></script>
        <script src="/public/js/respond.js"></script>
        <![endif]-->


        <!-- swiftype search box -->
        <script type="text/javascript">
            (function(w,d,t,u,n,s,e){w['SwiftypeObject']=n;w[n]=w[n]||function(){
                (w[n].q=w[n].q||[]).push(arguments);};s=d.createElement(t);
                e=d.getElementsByTagName(t)[0];s.async=1;s.src=u;e.parentNode.insertBefore(s,e);
            })(window,document,'script','//s.swiftypecdn.com/install/v2/st.js','_st');

            _st('install','TMVzky1S_9_z7jFBoarD','2.0.0');
        </script>
        <script src="<?php echo base_url(); ?>assets3/16d8cc00.js">
        </script>

        <script src="<?php echo base_url(); ?>assets3/commons_navigation.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>assets3/application_step1.js" type="text/javascript"></script>

        <script type="text/javascript">
            fbq('trackCustom', "Application_1");
        </script>


        <!-- Fine Uploader template
        ====================================================================== -->
        <script type="text/template" id="accomplishment-upload-template">
            <div class="qq-uploader-selector qq-uploader">
                <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                    <span>Drop files here to upload</span>
                </div>
                <span class="qq-drop-processing-selector qq-drop-processing invisible">
        <span>Processing dropped files...</span>
        <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
      </span>

                <div class="qq-upload-button-selector qq-upload-button app-btn">
                    <div>Upload a file</div>
                </div>

                <ul class="qq-upload-list-selector qq-upload-list">
                    <li>
                        <div class="qq-progress-bar-container-selector">
                            <div class="qq-progress-bar-selector qq-progress-bar"></div>
                        </div>

                        <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                        <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                        <span class="qq-edit-filename-icon-selector qq-edit-filename-icon"></span>
                        <span class="qq-upload-file-selector qq-upload-file"></span>
                        <span class="qq-upload-size-selector qq-upload-size"></span>
                        <a class="qq-upload-cancel-selector btn-small btn-warning" href="#">Cancel</a>
                        <a class="qq-upload-retry-selector btn-small btn-info" href="#">Retry</a>
                        <a class="qq-upload-delete-selector btn-small btn-warning" href="#">Delete</a> <span class="spacer"> | </span>
                        <a class="qq-upload-pause-selector btn-small btn-info" href="#">Pause</a>
                        <a class="qq-upload-continue-selector btn-small btn-info" href="#">Continue</a>
                        <span class="qq-upload-status-text-selector qq-upload-status-text"></span>
                        <a class="view-btn btn-small btn-info hide" target="_blank">View</a>
                    </li>
                </ul>
            </div>
        </script>

        <!-- Begin Inspectlet Embed Code -->
        <script id="inspectletjs" type="text/plain" data-cookieconsent="statistics">
    window.__insp = window.__insp || [];
    __insp.push(['wid', 799128071]);
    (function() {
      function ldinsp(){if(typeof window.__inspld != "undefined") return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
      setTimeout(ldinsp, 500); document.readyState != "complete" ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp();
    })();
  </script>
        <!-- End Inspectlet Embed Code -->

    </div></div>


<ul id="ui-id-1" tabindex="0" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="display: none;"></ul><div role="status" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div><div class="st-injected-content-generated st-install-TMVzky1S_9_z7jFBoarD" data-st-install-key="TMVzky1S_9_z7jFBoarD" id="st-injected-content">
    <div class="st-ui-embed st-search-chrome-overlay-output st-search-escape-hide-outputs st-search-zoom-on-mobile" style="display: none;">
        <div class="st-ui-overlay st-search-hide-outputs"></div>
        <!-- Swiftype input and results overlay -->
        <div class="st-ui-container st-ui-container-overlay  st-ui-injected-overlay-container ">
            <!-- HEADER -->
            <div class="st-ui-container-header_bar st-position-container">
                <section class="st-ui-header">
                    <form>
                        <input class="st-default-search-input st-search-set-focus" type="text" placeholder="Search this site" id="st-overlay-search-input" autocomplete="off" autocorrect="off" autocapitalize="off">
                    </form>
                    <span class="st-ui-search-icon"></span>

                    <a class="st-ui-close-button st-search-hide-outputs">Close</a>

                </section>
            </div>

            <!-- LEFT SIDEBAR -->
            <div class="st-ui-container-left_sidebar st-position-container">
                <div class="st-ui-injected-search-controls">
                </div>
            </div>

            <!-- RIGHT SIDEBAR -->
            <div class="st-ui-container-right_sidebar st-position-container">
                <div class="st-ui-injected-container-right_sidebar">
                    <div class="st-ui-injected-search-controls">
                    </div>
                </div>
            </div>

            <!-- PRIMARY CONTENT -->
            <div class="st-ui-container-primary_content st-position-container">
                <section class="st-ui-slide-autocomplete st-autocomplete-keyboard-navigable st-autocomplete-transient-on-select-and-results" data-st-target-element="#st-overlay-search-input" data-st-active-query-class="active">
                    <span class="st-ui-type-heading-small">suggested results</span>
                    <div class="st-autocomplete-results"><div class="st-query-present" style="display: none;"></div></div>
                </section>

                <section class="st-ui-content st-search-results"><div class="st-query-not-present">
                        <span class="st-ui-fallback"></span>
                    </div><div class="st-query-present" style="display: none;"></div></section>

                <section class="st-ui-no-results st-search-suggestions"><div class="st-query-not-present">
                    </div></section>
            </div>



            <!-- FOOTER -->
            <div class="st-ui-container-footer_bar st-position-container">
                <section class="st-ui-footer">
                    <span class="st-ui-search-summary st-search-summary"></span>
                    <span class="st-ui-pagination st-search-pagination"></span>
                </section>
            </div>
        </div>
    </div>
    <div class="st-default-autocomplete" data-st-target-element=".st-default-search-input" style="display: none;">
        <div class="st-autocomplete-results st-ui-autocomplete"><div class="st-query-present" style="display: none;"></div></div>
    </div>
</div></body></html>